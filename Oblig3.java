/*
 * Oblig 2 INF1010
 * Vaar 2014
 * Av Stian Kongsvik
 * stiako
 *
 */

import java.util.*;

class Oblig3{
    public static void main(String[] args){
        Test t = new Test();
    }
}
//Generisk klasse til å oppbevare objekter
@SuppressWarnings("unchecked")
class Container<T>{
    private T[] objects = null;
    private int antall = 0;

    Container (int lengde){
        objects = (T[]) new Object[lengde];
    }

    public int getAntall(){
        return antall;
    }


    public void set(T t){
        objects[antall] = t;
        antall++;
    }
    public T[] get(){
        return objects;
    }
    //Sjekker om samling er full
    public boolean full(){
        if (antall == objects.length){
            return true;
        }
        return false;
    }
}
//Klasse som tester at koden gjoer det den skal
class Test{
    Personer personer = new Personer();
    ListeAvPersoner liste = new ListeAvPersoner();
    GaveLager gl = new GaveLager(500);
    Gave gave;
    Person person;
    String [] navn = personer.hentPersonnavn();

    Test(){
        person = personer.hentPerson();
        settInnILenkeliste();
        gaveDryss();
    }

    public void settInnILenkeliste(){
        while(person != null){
            liste.settInnSist(person);
            person = personer.hentPerson();
        }
    }

    public void gaveDryss(){
        gave = gl.hentGave();
        while(gave != null){
            for (int i = 0; i < navn.length; i++){
                gave = liste.finnPerson(navn[i]).vilDuHaGaven(gave);
                if(gave == null) break;
            }
            gave = gl.hentGave();
        }
        liste.skrivAlle();
    }
}

//Klassen "Person" inneholder alle metoder for aa knytte personobjektene sammen. 
@SuppressWarnings("unchecked")
class Person{
    private String navn;  
    private int samlingsStorrelse;
    private Person[] kjenner;
    private Person[] likerikke;
    // likerikke = uvenner 
    // venner blir da alle personer som pekes paa av kjenner 
    // unntatt personen(e) som pekes paa av likerikke
    private Person forelsketi;
    private Person sammenmed;
    //Variabler som inneholder interesser til person-objekt
    private String artist;
    private int eldreEnn;
    private String smlp = null;
    //HashMap for aa oppbevare plate- og boksamlinger
    private HashMap<String, Container> samlinger = new HashMap<String, Container>();
    public Person neste;
    private Gave[] mineGaver;
    private int antGaver = 0;
    private int antVenner = 0;

    //Konstruktor for klassen Person. Her settes navn og hva personen samler paa. 
    Person(String navn, String smlp, int antall){
        this.navn = navn;
        samlerAv(smlp, antall);
    }
    //Konstruktoer for denne oppgaven, her settes navn og antall venner og personer en ikke liker	
    Person(String navn){
	      this.navn = navn;
	      this.kjenner = new Person[100];
        this.likerikke = new Person[10];
    }

    //Opprettet samlinger i et HashMap
    public void samlerAv(String s, int ant){
        smlp = s;
        mineGaver = new Gave[ant];
    }

    //Lagrer interessene basert paa parameter-type
    public void megetInteressertI(String artist){
        this.artist = artist;
    }
    public void megetInteressertI(int eldreEnn){
        this.eldreEnn = eldreEnn;
    }

    //Metode for aa fordele gaver
    public Gave vilDuHaGaven(Gave g){
        //Tar imot gave om kriteriene oppfylles
        if (smlp.equals(g.kategori()) && antGaver < mineGaver.length){
            mineGaver[antGaver] = g;
            antGaver++;
            return null;
        } else {
            //Gir videre til relasjoner i prioritert rekkefolge om den ikke blir tatt imot av denne personen
            if (sammenmed != null){
                g = sammenmed.giVidere(g);
                if(g == null) return g;
            } else if (forelsketi != null){
                g = forelsketi.giVidere(g);
                if (g == null) return g;
            } else {
                for (int i = 0; i < antVenner; i++){
                    if (erVennMed(kjenner[i])){
                        g = kjenner[i].giVidere(g);
                        if (g == null) return g;
                    }
                }
                return g;
            }
        }
		    return g;
    }
    //Metode for aa gi videre gaver for aa unngaa uendelig loop
    public Gave giVidere(Gave g){
        if (g.kategori().equals(smlp) && antGaver < mineGaver.length){
            mineGaver[antGaver] = g;
            antGaver++;
            return null;
        }
        return g;
    }

    //Denne metoden brukes kun i utskrift, for aa hente navn til de ulike personobjektene
    public String hentNavn(){
        return navn;
    }

    //Metode som sjekker om to personobjekter kjenner hverandre. Returnerer en boolsk verdi.
    public boolean erKjentMed(Person p){
	      for(int i = 0;i<p.kjenner.length; i++){
	          if(kjenner[i] == p){
		        return true;
	          }
	      }
	      return false;
    }
    //Metode som sjekker og eventuelt legger til bekjentskaper mellom personobjektene. 
    //Personobjektene legges da til i "kjenner-arrayet".
    public void blirKjentMed(Person p){
	      if(navn.equals(p.navn)){
	          System.out.println("Kan ikke bli kjent med seg selv");
	      } else if(erKjentMed(p)){
	          System.out.println("Kjenner allerede denne personen");
	      }else {
	          for(int i = 0;i<kjenner.length;i++){
		            if(kjenner[i] == null){
		                kjenner[i] = p;
                    antVenner++;
		                break;
		            }
	          }
	      }
    }

    //Gir et objekt til pekeren "forelsketi". Sjekker ogsaa om instansen er det samme som objektet som blir sjekket.
    public void blirForelsketI(Person p) {
	      if(this != p && !likerIkke(p)){
	          forelsketi = p;
	      } else {
	          System.out.println("Kan ikke vaere forelsket i seg selv");
	      }
    }
    //Metode for aa sette sammenmed
    public void blirSammenMed(Person p){
        if (p != this){
            p.settSammen(this);
            sammenmed = p;
        }
    }

    //Metode for aa sette den andre personen sammen med denne
    public void settSammen(Person p){
        sammenmed = p;
    }

    //Samme som "blirKjentMed", men her legges objektet inn i "likerikke-arrayet".
    //Sjekker ogsaa om objektet allerede ligger i arrayet.
    public void blirUvennMed(Person p){
	      boolean finnes = true;
	      if(navn.equals(p.navn)){
	          System.out.println("Kan ikke bli uvenn med seg selv");
	      }else{
	          for(int i = 0;i<likerikke.length;i++){
		            if(likerikke[i] == p){
		                System.out.println("Er allerede uvenn med denne personen");
		            }else{
		                finnes = false;
		            }
	          }
	      }
	      //Her legges personobjektet til i "likerikke-arrayet", dersom det ikke allerede er der.
        if(!finnes){
	          for(int i = 0;i<likerikke.length;i++){
		            if(likerikke[i] == null) {
		                likerikke[i] = p;
		                break;
		            }
	          }
	      }
    }

    //Sjekker om et personobjeket er venn med et annet.
    //Sjekker forst om de kjenner hverandre, via "kjenner-arrayet".
    //Deretter sjekkes "likerikke-arrayet". Dersom personobjektet ligger i sistnevnte kan de ikke vaere venner.
    public boolean erVennMed(Person p){
	      boolean venn = false;
	      for(int i = 0;i < p.kjenner.length;i++){
	          if(kjenner[i] == p){
		            venn = true;
	          }
        }
        for (int i = 0; i < p.likerikke.length;i++){
	          if(likerikke[i] == p){
		            venn = false;
	          }
	      }
	      return venn;
    }

    //Sjekker metoden "erVennMed". Dersom de allerede er venner kan de ikke bli det paa nytt.
    //Sjekker deretter "likerikke-arrayet". 
    //Dersom personobjektet finnes der blir plassen i arrayet satt til null
    public void blirVennMed(Person p){
	      if(!erVennMed(p) && p != this){
	          for(int i = 0;i<likerikke.length;i++){
		            if(likerikke[i] == p){
		                likerikke[i] = null;
		            }
	          }
	      }
    }

    //Metode som skriver ut vennene for et bestemt personobjekt, fra metoden "hentVenner".
    public void skrivUtVenner(){
	      System.out.println(hentVenner());
    }

    //Henter bestevenn som blir returnert som en string.
    //Bestevenn er for enkelhetens skyld den kjenningen som ligger paa plass "0" i kjenner-arrayet.
    public String hentBestevenner(){
	      return kjenner[0].navn;
    }

    //henter venner fra metoden "erVennMed". Legger til nanvene i en String og returnerer denne Stringen.
    public String hentVenner(){
	      String venner = "";
	      for(int i = 0;i<kjenner.length;i++){
	          if(erVennMed(kjenner[i])){
		            venner += kjenner[i].navn + " ";
	          }
	      }
	      return venner;
    }
    //Returnerer antall venner for et personobjekt
    //Sjekker metoden "erVennMed" og plusser "int antall" per venn.
    public int antVenner(){
	      int antall = 0;
	      for(int i = 0;i<kjenner.length;i++){
	          if(erVennMed(kjenner[i]))
		            antall++;
	      }
	      return antall;
    }
    //Sjekker om person er i likerikke[]
    private boolean likerIkke(Person p){
        for(int i = 0; i < likerikke.length; i++){
            if(p == likerikke[i]){
                return true;
            }
        }
        return false;
    }

    //Skriver ut alle navn til objekter som befinner seg i "kjenner-arrayet".
    public void skrivUtKjenninger(){
	      for (Person p: kjenner){
	          if (p!=null){
		            System.out.print(p.hentNavn() + " ");
	          }
	      }
	      System.out.println("");
    }
    //Samme som metoden over, men for "likerikke-arrayet".
    public void skrivUtLikerIkke(){
	      for (Person p: likerikke) {
	          if (p!=null){
		            System.out.print(p.hentNavn() + " ");
	          }
	      }
	      System.out.println("");
    }
    //Her hentes og samles all informasjon om personobjektet, fOr det skrives ut. 
    //Denne metoden bruker "skrivUtKjenninger" og "skrivUtLikerIkke".
    //Denne metoden skriver altsaa ut alle relasjoner for et personobjekt. 
    public void skrivUtAltOmMeg(){
	      boolean mislikerNoen = false;

	      System.out.print(navn + " kjenner: ");
	      skrivUtKjenninger();

        System.out.println();

        if (sammenmed != null){
            System.out.println(navn + " er sammen med " + sammenmed.hentNavn());
        }

	      if(forelsketi!= null){
	          System.out.println(navn + " er forelsket i " + forelsketi.hentNavn());
	      }

	      for(int i = 0;i<likerikke.length;i++) {
	          if(likerikke[i] != null){
		            mislikerNoen = true;
	          }
	      }

	      if(mislikerNoen){
	          System.out.print(navn + " liker ikke: ");
	          skrivUtLikerIkke();
	      }

        if(smlp != null){
            System.out.println(navn + " samler paa: " + smlp);
        } else {
            System.out.println(navn + " samler ikke paa noe.");
        }

        if (antGaver == 0){
            System.out.println(navn + " har ikke mottatt noen gaver");
        } else {
            for (int i = 0; i < antGaver; i++){
                System.out.println(navn + " har mottatt: " + mineGaver[i].gaveId());
            }
        }
        System.out.println("----------------------------");
    }
}
